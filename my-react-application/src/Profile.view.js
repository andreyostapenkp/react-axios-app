import React from 'react';

class ProfileView extends React.Component {

    render() {
        return (
        <tr>
              {[
                <td><span>{this.props.person.name}</span></td>,
                <td><span>{this.props.person.age}</span></td>,
                <td><span>{this.props.person.address}</span></td>,
                <td><button onClick={(e)=>this.props.removeEvent(this.props.person.id)}> Remove </button></td>
              ]}
        </tr>
        )
      }
}

export default ProfileView;