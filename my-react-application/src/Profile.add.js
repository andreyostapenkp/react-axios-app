import React from 'react';

class ProfileAdd extends React.Component {
  constructor(props){
    super(props);

    this.name = "";
    this.age = "";
    this.address = "";
  }

    render() {
        return (
        <tr>
              {[
                <td><input id='input-profile-name' onChange={(e)=>{ this.name = e.target.value; }} /></td>,
                <td><input id='input-profile-age' type="number" min="18" max="100" onChange={(e)=>{ this.age = e.target.value; }}/></td>,
                <td><input id='input-profile-address' onChange={(e)=>{ this.address = e.target.value; }}/></td>,
                <td><button onClick={(e)=>{this.props.addEvent(this.name, this.age, this.address); this.reset()} }> Add </button></td>
              ]}
        </tr>
        )
      }

      reset() { 
        document.getElementById("input-profile-name").value = "";
        document.getElementById("input-profile-age").value = "";
        document.getElementById("input-profile-address").value = "";
      }
    }

export default ProfileAdd;