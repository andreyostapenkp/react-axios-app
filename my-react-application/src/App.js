import React from 'react';
//import logo from './logo.svg';
import './App.css';
import axios from 'axios';
import ProfileView from './Profile.view'
import ProfileAdd from './Profile.add'
import 'bootstrap/dist/css/bootstrap.css';

 class App extends React.Component {

  constructor(props)
  {
    super(props);
    this.state = {
      persons: []
    }
  }

  componentDidMount() {
    this.loadProfiles();
  }

  render() {
    return (
      <div className="container">
        <h2>Hover Rows</h2>
        <p>The .table-hover class enables a hover state on table rows:</p>    
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Name</th>
              <th>Age</th>
              <th>Address</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            { this.state.persons.map(person => <ProfileView person = {person}  removeEvent = { (id) => this.removeProfile(id) } key={person.id}/>)}
            <ProfileAdd addEvent = { (name, age, address) => this.addProfile(name,age,address) }/>
          </tbody>
        </table>
      </div>
    )
  }

  loadProfiles()
  {
    axios.get('https://localhost:5001/api/values')
    .then(res => {
      const persons = res.data;
      this.setState({ persons });
    })
  }

  addProfile(name, age, address)  {
    axios.put('https://localhost:5001/api/values/', { name: name, age: age, address: address})
    .then(res => {this.loadProfiles() });
  }

  removeProfile(id)  {
    axios.delete('https://localhost:5001/api/values/'+id)
    .then(res => {this.loadProfiles() });
  }
}

export default App;
