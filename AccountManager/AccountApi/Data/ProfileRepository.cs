﻿using AccountApi.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountApi.Data
{
    public class ProfileRepository
    {
        private List<Profile> _profiles;
        private int _indexId;

        public ProfileRepository()
        {
            InitRepository();
        }

        public IEnumerable<Profile> GetAll()
        {
            return _profiles;
        }

        public void Add(string name, int age, string address)
        {
            _profiles.Add(new Profile
            {
                Id = _indexId++,
                Name = name,
                Age = age,
                Address = address

            });
        }

        public void Remove(int id)
        {
            var item = _profiles.FirstOrDefault(s => s.Id == id);
            if (item != null)
            {
                _profiles.Remove(item);
            }
        }

        private void InitRepository()
        {
            _profiles = new List<Profile>();

            _profiles.AddRange(
                new[] {
                    new Profile{
                        Id = _indexId++,
                        Name = "Nick",
                        Age = 29,
                        Address = "City A"

                },

                new Profile {
                     Id = _indexId++,
                     Name = "Anrii",
                     Age = 29,
                     Address = "City B"

                },
                new Profile {
                     Id = _indexId++,
                     Name = "Alex",
                     Age = 29,
                     Address = "City C"

                },
                new Profile {
                     Id = _indexId++,
                     Name = "Emma",
                     Age = 29,
                     Address = "City D"

                },
                new Profile {
                     Id = _indexId++,
                     Name = "Make",
                     Age = 29,
                     Address = "City E"
                },
            });
        }
    }
}
