﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccountApi.Data;
using AccountApi.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace AccountApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly ProfileRepository _profileRepository;

        public ValuesController(ProfileRepository profileRepository)
        {
            _profileRepository = profileRepository;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Profile>> Get()
        {
            return _profileRepository.GetAll().ToList();
        }

        // PUT api/values/5
        [HttpPut]
        public void Put([FromBody] ProfileRequest request)
        {
            _profileRepository.Add(request.Name, request.Age, request.Address);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _profileRepository.Remove(id);
        }
    }


}
